package elivator

object Main extends App {
  val elf = new SimpleElivatorFactory(1, 3)
  val eles = List(
    elf.create(ElivatorId("A")),
    elf.create(ElivatorId("B"))
  )
  val system = new SimpleElivatorSystemBuilder()
    .withElivators(eles)
    .withFloorsNumpad((1 to 10).toList)
    .build

  system.numpads(3).click(7)
  system.numpads(1).click(9)
  println(eles)
}


case class Route(from: Int, to: Int)

case class ElivatorId(id: String)

trait ElivatorsManager {
  def assignRouteToElivator(route: Route): ElivatorId
}

trait Elivator {
  def id: ElivatorId

  def assignRoute(route: Route): Unit

  def rankAvailabilityForRoute(route: Route): Int
}

trait ElivatorFactory {
  def create(id: ElivatorId): Elivator
}

trait Numpad {
  def click(clickedFloor: Int): ElivatorId
}

class FloorNumPad(floor: Int, handler: Route => ElivatorId) extends Numpad {
  def click(clickedFloor: Int): ElivatorId = {
    handler(Route(floor, clickedFloor))
  }
}

class FloorNumpadFactory(handler: Route => ElivatorId) {
  def create(floor: Int): FloorNumPad = new FloorNumPad(floor, handler)
}

abstract class ElivatorWithMergeAndRank(val identifier: ElivatorId, startFloor: Int = 0) extends Elivator {
  private var path = List(startFloor)

  val id: ElivatorId = identifier

  override def assignRoute(route: Route): Unit = {
    path = mergeRouteInPath(path, route)
  }

  def mergeRouteInPath(path: List[Int], route: Route): List[Int]

  def rankAvailabilityForRoute(route: Route): Int = rankByRouteAndPath(route, path)

  def rankByRouteAndPath(route: Route, path: List[Int]): Int

  def getCurrentFloor: Int = path.head

  def nextFloor(): Unit = {
    if (path.size <= 1)
      throw new PathIsEmptyError()
    path = path.slice(1, path.size)
  }

  override def toString: String = s"$id  $path"
}

trait MergeRoute {
  def mergeRouteInPath(path: List[Int], route: Route): List[Int]
}

trait RankAvailability {
  def rankByRouteAndPath(route: Route, path: List[Int]): Int
}

trait MergeRouteToTheEnd extends MergeRoute {
  def mergeRouteInPath(path: List[Int], route: Route): List[Int] = {
    path ++ List(route.from, route.to)
  }
}

trait SimpleRankAvailability {
  def floorsInPathMultiplier: Int

  def distanceFromLastFloorMultiplier: Int

  def rankByRouteAndPath(route: Route, path: List[Int]): Int = {
    Math.abs(route.from - path.last) * distanceFromLastFloorMultiplier + path.size * floorsInPathMultiplier
  }
}

class SimpleElivator(identifier: ElivatorId, val floorsInPathMultiplier: Int, val distanceFromLastFloorMultiplier: Int, startFloor: Int = 0)
  extends ElivatorWithMergeAndRank(identifier, startFloor)
    with MergeRouteToTheEnd
    with SimpleRankAvailability {
}

class SimpleElivatorFactory(val floorsInPathMultiplier: Int, val distanceFromLastFloorMultiplier: Int, startFloor: Int = 0)
  extends ElivatorFactory {
  override def create(id: ElivatorId): Elivator = new SimpleElivator(id, floorsInPathMultiplier, distanceFromLastFloorMultiplier, startFloor)
}

class PathIsEmptyError extends Error {}

class SimpleElivatorsManager(elivators: List[Elivator]) extends ElivatorsManager {
  private def pickElivatorByRoute(route: Route): Elivator = {
    elivators.map(e => (e, e.rankAvailabilityForRoute(route))).minBy(_._2)._1
  }

  def assignRouteToElivator(route: Route): ElivatorId = {
    val chosenElivator = pickElivatorByRoute(route)

    chosenElivator.assignRoute(route)

    chosenElivator.id
  }
}

class SimpleElivatorSystemBuilder() {
  private var floorsToCreate = List[Int]()

  def withFloorsNumpad(floors: List[Int]): SimpleElivatorSystemBuilder = {
    floorsToCreate = floorsToCreate ++ floors
    this
  }

  private var eles = List[Elivator]()

  def withElivators(elivators: List[Elivator]): SimpleElivatorSystemBuilder = {
    eles = eles ++ elivators
    this
  }

  def build: SimpleElivatorSystem = {
    val manager = new SimpleElivatorsManager(eles)

    val numpadFactory = new FloorNumpadFactory(manager.assignRouteToElivator)
    val numPads = floorsToCreate.map(numpadFactory.create)

    new SimpleElivatorSystem(numPads)
  }

}

class SimpleElivatorSystem(val numpads: List[Numpad]) {

}
